#Prerequisites
download kafka at  https://www.apache.org/dyn/closer.cgi?path=/kafka/0.11.0.1/kafka_2.11-0.11.0.1.tgz
extract kafka in a folder

#Kafka commands

## Start Zookeeper
bin/zookeeper-server-start.sh config/zookeeper.properties

## Start Kafka
bin/kafka-server-start.sh config/server.properties

## Create input topic
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic word-count-input

## Create output topic
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic word-count-output

## Start a kafka producer
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic streams-plaintext-input

...enter some text:
kafka streams sample
kafka data processing
kafka streams is awesome

## Display the data in a topic to the console
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic word-count-input --from-beginning

# Start a consumer on the output topic
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 \
    --topic word-count-output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer

# Start the streams application
java -jar target/streams-starter-project-1.0.0-SNAPSHOT-jar-with-dependencies.jar
bin/kafka-run-class.sh org.bitbucket.cbrink.kafka.streams.StreamStarterApp
